<?php

/**
 * @file
 * Handles request to AvaTax for sales tax values.
 */

/**
 * Makes a request to Avatax for sales tax values.
 *
 * @param object $order
 *   The current order object.
 * @param array $ava_args
 *   An array containing order delivery details.
 *
 * @return bool|array
 *   FALSE if the tax calculation failed.
 *   An array containing tax amount, taxable amount, total order amount.
 */
function uc_avatax_get_tax($order, $ava_args) {

  $request_body = array(
    'companyCode' => $ava_args['company_code'],
    'type' => $ava_args['doc_type'],
    'code' => $ava_args['doc_code_prefix'] . '-' . $order->order_id . '',
    'date' => date("Y-m-d", $ava_args['doc_date']),
    'customerCode' => $ava_args['user_id'],
    'customerUsageType' => $ava_args['avatax_exemption_code'],
    'commit' => $ava_args['commit'],
    'addresses' => array(
      // Origin.
      'ShipFrom' => array(
        'line1' => $ava_args['primary_street1'],
        'line2' => $ava_args['primary_street2'],
        'city' => $ava_args['primary_city'],
        'region' => $ava_args['primary_state'],
        'country' => $ava_args['primary_country'],
        'postalCode' => $ava_args['primary_zip'],
      ),
      // Destination.
      'ShipTo' => array(
        'line1' => $ava_args['street1'],
        'line2' => $ava_args['street2'],
        'city' => $ava_args['city'],
        'region' => $ava_args['state'],
        'country' => $ava_args['country'],
        'postalCode' => $ava_args['zip'],
      ),
    ),
  );

  $i = 1;
  foreach ($order->products as $prod) {
    $tax_code = '';
    if ($ava_args['product_version'] == UC_AVATAX_PRO_VERSION) {
      if (isset($prod->field_avatax_code)) {
        $avatax_array = $prod->field_avatax_code;
        if ($avatax_array) {
          $tid = $avatax_array['und'][0]['tid'];
          $taxonomy_term = taxonomy_term_load($tid);
          $tax_code = $taxonomy_term->name;
        }
      }
      else {
        $product = node_load($prod->nid);
        if (isset($product->field_avatax_code)) {
          $avatax_array = $product->field_avatax_code;
          if ($avatax_array) {
            $tid = $avatax_array['und'][0]['tid'];
            $taxonomy_term = taxonomy_term_load($tid);
            $tax_code = $taxonomy_term->name;
          }
        }
      }
    }
    $lines[] = array(
      'number' => $i,
      'itemCode' => $prod->model,
      'description' => $prod->title,
      'taxCode' => $tax_code,
      'quantity' => $prod->qty,
      'amount' => $prod->qty * $prod->price,
      'discounted' => 'true',
    );
    $i++;
  }

  foreach ($order->line_items as $key => $item) {
    if (in_array($item['type'], array('coupon'))) {
      $lines[] = array(
        'number' => $i,
        'itemCode' => $item['type'],
        'Description' => $item['title'],
        'quantity' => 1,
        'amount' => $item['amount'],
        'discounted' => 'false',
      );
      $i++;
    }
    elseif (in_array($item['type'], array('shipping'))) {
      $lines[] = array(
        'number' => $i,
        'itemCode' => $item['type'],
        'description' => $item['title'],
        'taxCode' => $ava_args['shipcode'],
        'quantity' => 1,
        'amount' => $item['amount'],
        'discounted' => 'false',
      );
      $i++;
    }
    elseif (in_array($item['type'], array('generic'))) {
      $lines[] = array(
        'number' => $i,
        'ItemCode' => $item['type'],
        'description' => $item['title'],
        'quantity' => 1,
        'amount' => $item['amount'],
        'discounted' => 'false',
      );
      $i++;
    }
    elseif (in_array($item['type'], array('uc_discounts'))) {
      $request_body['Discount'] = (round(($item['amount']), 2)) * -1;
    }
  }
  $request_body['lines'] = $lines;

  $response = uc_avatax_post('/transactions/create', $request_body);
  if (is_array($response) && $response['body']) {
    $result = $response['body'];
    if (!isset($result['error'])) {
      $tax_data = array(
        'tax_amount' => $result['totalTax'],
        'taxable_amount' => $result['totalTaxable'],
        'total_amount' => $result['totalAmount'],
      );
      return $tax_data;
    }
    else {
      drupal_set_message(t('AvaTax error: %type - %target - %summary', array(
        '%type' => isset($result['error']['code']) ? $result['error']['code'] : 'No code given',
        '%target' => isset($result['error']['target']) ? $result['error']['target'] : 'No target given',
        '%summary' => isset($result['error']['message']) ? $result['error']['message'] : 'No message given',
      )), 'error');
      return FALSE;
    }
  }

  if (!$response) {
    drupal_set_message(t("AvaTax did not get any response."), 'error');
    return FALSE;
  }
}
