<?php

/**
 * @file
 * Rules supporting AvaTax Sales Order Processing.
 */

/**
 * Implements hook_rules_action_info().
 */
function uc_avatax_rules_action_info() {
  $parameter = array(
    'order' => array(
      'type' => 'uc_order',
      'label' => t('Order'),
    ),
  );
  $actions = array(
    'uc_avatax_commit_transaction' => array(
      'label' => t('Change status of sales tax to COMMITTED in AvaTax'),
      'group' => t('AvaTax'),
      'parameter' => $parameter,
    ),
    'uc_avatax_cancel_transaction' => array(
      'label' => t('Change status of sales tax to CANCELLED in AvaTax'),
      'group' => t('AvaTax'),
      'parameter' => $parameter,
    ),
  );

  return $actions;
}

/**
 * COMMIT AvaTax transaction for $order.
 */
function uc_avatax_commit_transaction($order) {
  _uc_avatax_update($order, 'commit');
}

/**
 * DELETE AvaTax transaction for $order.
 */
function uc_avatax_cancel_transaction($order) {
  _uc_avatax_update($order, 'cancel');
}

/**
 * Send Commit/Cancel operation to AvaTax.
 */
function _uc_avatax_update($order, $type = 'commit') {
  // Get Company code and Company Use Mode.
  $company_code = (variable_get('uc_avatax_company_code', ''));
  $use_mode = (variable_get('uc_avatax_use_mode', ''));
  $product_version = variable_get('uc_avatax_product_version', 'trial');
  $account_no = variable_get('uc_avatax_' . $product_version . '_' . $use_mode . '_account');
  $license_key = variable_get('uc_avatax_' . $product_version . '_' . $use_mode . '_license');

  $doc_code_prefix = 'uc';

  $order_wrapper = entity_metadata_wrapper('uc_tax_order', $order);
  if (!uc_avatax_check_address($order)) {
    return;
  }
  // Attempt to fetch existing transaction from avatax.
  $transaction_status = uc_avatax_get_transaction_status($order);
  switch ($type) {
    case 'commit':

      switch ($transaction_status) {
        case 'false':
          // Create SalesInvoice in AvaTax, committed.
          $amt = uc_avatax_retrieve_sales_tax($order, "SalesInvoice", TRUE);
          if (!$amt) {
            drupal_set_message(t('Failed to create order %oid in AvaTax.', array('%oid' => $order->order_id)), 'status');
            watchdog('uc_avatax', 'Failed to create SalesInvoice in AvaTax for order %order', array('%order' => $order->order_id), WATCHDOG_ERROR);
          }
          else {
            // Order created successfully.
            drupal_set_message(t('Order %oid created and committed in AvaTax.', array('%oid' => $order->order_id)), 'status');
          }
          break;

        case 'voided':
          // Voided in AvaTax.
          drupal_set_message(t('Order %oid AvaTax transaction has been previously voided, and cannot be committed.', array('%oid' => $order->order_id)), 'status');
          break;

        case 'locked':
          // Locked in AvaTax, cannot be changed.
          drupal_set_message(t('Order %oid has been reported to tax authority and can no longer be modified.', array('%oid' => $order->order_id)), 'error');
          break;

        default:
          $body = array(
            'commit' => 'true',
          );
          $response = uc_avatax_post('/companies/' . $company_code . '/transactions/' . $doc_code_prefix . '-' . $order->order_id . '/commit', $body);
          if (is_array($response) && $response['body']) {
            $result = $response['body'];
            if (!isset($result['error'])) {
              drupal_set_message(t('Order %oid AvaTax transaction committed.', array('%oid' => $order->order_id)), 'status');
              return;
            }
            else {
              drupal_set_message(t('AvaTax error: %type - %target - %summary', array(
                '%type' => isset($result['error']['code']) ? $result['error']['code'] : 'No code given',
                '%target' => isset($result['error']['target']) ? $result['error']['target'] : 'No target given',
                '%summary' => isset($result['error']['message']) ? $result['error']['message'] : 'No message given',
              )), 'error');
              watchdog('uc_avatax', 'Failed to commit order @id !req !resp', array(
                '@id' => $order->order_id,
                '!req' => '<pre>' . var_export($body, TRUE) . '</pre>',
                '!resp' => '<pre>' . var_export($response, TRUE) . '</pre>',
              ), WATCHDOG_ERROR);
              return;
            }
          }

          if (!$response) {
            drupal_set_message(t("AvaTax did not get a response."), 'error');
            watchdog('uc_avatax', "Failed to commit order @id - AvaTax did not respond.", array('@id' => $order->order_id), WATCHDOG_ERROR);
            return;
          }
      }

    case 'cancel':

      switch ($transaction_status) {
        case 'false':
          // Doesn't exist in AvaTax, do nothing.
          break;

        case 'voided':
          // Voided in AvaTax.
          drupal_set_message(t('Order %oid AvaTax transaction has been previously voided.', array('%oid' => $order->order_id)), 'status');
          break;

        case 'locked':
          // Locked in AvaTax, cannot be changed.
          drupal_set_message(t('Order %oid has been reported to tax authority and can no longer be modified.', array('%oid' => $order->order_id)), 'error');
          break;

        default:
          $body = array(
            'code' => 'DocVoided',
          );
          $response = uc_avatax_post('/companies/' . $company_code . '/transactions/' . $doc_code_prefix . '-' . $order->order_id . '/void', $body);
          if (is_array($response) && $response['body']) {
            $result = $response['body'];
            if (!isset($result['error'])) {
              drupal_set_message(t('Order %oid AvaTax transaction voided.', array('%oid' => $order->order_id)), 'status');
              return;
            }
            else {
              drupal_set_message(t('AvaTax error: %type - %target - %summary', array(
                '%type' => isset($result['error']['code']) ? $result['error']['code'] : 'No code given',
                '%target' => isset($result['error']['target']) ? $result['error']['target'] : 'No target given',
                '%summary' => isset($result['error']['message']) ? $result['error']['message'] : 'No message given',
              )), 'error');
              watchdog('uc_avatax', 'Failed to void order @id !req !resp', array(
                '@id' => $order->order_id,
                '!req' => '<pre>' . var_export($body, TRUE) . '</pre>',
                '!resp' => '<pre>' . var_export($response, TRUE) . '</pre>',
              ), WATCHDOG_ERROR);
              return;
            }
          }

          if (!$response) {
            drupal_set_message(t("AvaTax did not get a response."), 'error');
            watchdog('uc_avatax', "Failed to void order @id - AvaTax did not respond.", array('@id' => $order->order_id), WATCHDOG_ERROR);
            return;
          }
      }
  }
}
